const mongoose = require("mongoose")

const employSchema = new mongoose.Schema(
    {
    name :{
        type: String,
        trim: true,
        required:true,
        maxlength: 32,
        //unique: true
     },
    designation :{
        type: String,
        trim: true,
        //required:true,
        maxlength: 32,
        unique: true
     },
     monthlySalary:{
        type: Number,
        trim: true,
        default:0,
       // required:true,
        maxlength: 82,
     },
     perdaySalary :{
        type: Number,
        trim: true,
        default:0,
       // required:true,
        maxlength: 82,
     },
     dutyTime :{
        type: Number,
        trim: true,
        default:0,
      //  required:true,
        maxlength: 82,
     }
    }
    ,{timestamps: true} 
);

module.exports = mongoose.model("Employ",employSchema);