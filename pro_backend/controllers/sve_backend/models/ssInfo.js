const mongoose = require("mongoose")

const employSchema = new mongoose.Schema(
    {
        employeID :{
        type: String,
        trim: true,
        required:true,
        maxlength: 32,
     },
     workingHour :{
        type: Number,
        trim: true,
        maxlength: 100
     },
     otherWorkingHour :{
        type: Number,
        trim: true,
        maxlength: 100
     },
     allowLeaves:{
        type: Number,
        trim: true,
        maxlength: 100
     },
     holiday:{
        type: Number,
        trim: true,
        maxlength: 100
     },
     salaryAdvance:{
        type: Number,
        trim: true,
        maxlength: 100
     },
     otherAdvance:{
        type: Number,
        trim: true,
        maxlength: 100
     },

     month :{
        type: String,
        trim: true,
        maxlength: 100
     }
    }
)