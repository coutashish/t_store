const express = require("express");
const router = express.Router();
const { createEmploy, getEmployById, getAllEmploy, getEmploy, updateEmploy, removeEmploy } = require("../controllers/employ")

router.param("employID",getEmployById);

router.get( "/employ/:employID", getEmploy);
router.get( "/employs", getAllEmploy);
router.post("/employ/createNew",createEmploy);
router.put("/employ/update/:employID",updateEmploy);
router.delete("/employ/remove/:employID",removeEmploy);


module.exports = router;