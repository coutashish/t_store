const Employ = require("../models/employ")

exports.createEmploy = (req, res) => {
    const employ = new Employ(req.body);
    employ.save((err, employ) => {
        if (err) {
         
            return res.status(400).json({
                error: "not able to save category  in db"
            });
        }
        res.json({
            employ
        });
    });
};

exports.getEmployById = (req, res, next, id) => {

    Employ.findById(id).exec((err, emp) => {
        if (err) {
            return res.status(400).json({
                error: "Employ not found in db"
            });
        }
        req.employ = emp;
        next();
    });
};

exports.getEmploy = (req, res) => {
    return res.json(req.employ);
};

exports.getAllEmploy = (req, res) => {
    Employ.find().exec((err, employes) => {
        if (err) {
            return res.status(400).json({
                error: "employs not found "
            });
        }
        res.json(employes);
    })
};

exports.updateEmploy = (req, res) => {
    const employ = req.employ;
    employ.name = req.body.name;

    employ.save((err, updatedEmploy) => {
        if (err) {
           
            return res.status(400).json({
                error: "fail to save update"
               
            });
        }
        res.json(updatedEmploy);
    });
};

exports.removeEmploy = (req, res) => {
    const employ = req.employ;
    employ.remove((err, employ) => {
        if (err) {
            return res.status(400).json({
                error: "failed to remove this employ"
            });
        }
        res.json({
            message: "successfully deleted"
        });
    });
};