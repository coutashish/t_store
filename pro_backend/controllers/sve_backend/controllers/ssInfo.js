const Ssinfo = require("../models/ssInfo");

exports.createSsinfo = (req,res)=>{
    const ssinfo = new Ssinfo(req.body);
    ssinfo.save((err,ssinfo)=>{
        if(err){
            return res.status(400).json({
                error:"not able to create"
            });
        }
        res.json({
            ssinfo
        })
    });
};

exports.getSalaryById = (req, res, next, id) => {

    Ssinfo.findById(id).exec((err, emp) => {
        if (err) {
            return res.status(400).json({
                error: "Employ not found in db"
            });
        }
        req.ssinfo = emp;
        next();
    });
};

exports.getSsinfo = (req, res) => {
    return res.json(req.ssinfo);
};

exports.getAllSsinfo = (req, res) => {
    Ssinfo.find().exec((err, ssinfo) => {
        if (err) {
            return res.status(400).json({
                error: "no salary info found "
            });
        }
        res.json(ssinfo);
    })
};

