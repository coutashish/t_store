const mongoose = require("mongoose");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const userRoutes = require("./routes/employ")


//database connections

    mongoose.connect("mongodb://localhost:27017/sve",{
        useNewUrlParser: true,
        useUnifiedTopology:true
    }).then(()=>console.log('Mongo connection esteblish'))
      .catch((error)=> console.error("MongoDb connection failed to establish")) 

//database connection

//MiddleLayer

    app.use(bodyParser.json());

// MiddleLayer

//my routes
app.use("/api", userRoutes);
//my routes


//server
const port = 8000;
app.listen(port,()=>{
    console.log(`app is running at ${port}`);
})